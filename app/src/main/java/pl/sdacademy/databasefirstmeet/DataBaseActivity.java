package pl.sdacademy.databasefirstmeet;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DataBaseActivity extends AppCompatActivity {

    String title;
    int monye;
    String date;
    String rating;

    RecyclerView myRecycler;
    LinearLayoutManager llm;
    MovieAdapter adapter;
    DataBaseQueries polaczenieBazy;

    List<MovieModel> movieModelList;


    @OnClick(R.id.fab)
    public void addMovie() {

        LinearLayout layout = new LinearLayout(DataBaseActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText dialogTitle = new EditText(DataBaseActivity.this);
        dialogTitle.setHint("Tytuł");
        layout.addView(dialogTitle);

        final EditText dialogMoney = new EditText(DataBaseActivity.this);
        dialogMoney.setHint("Budżet");
        layout.addView(dialogMoney);

        final EditText dialogDate = new EditText(DataBaseActivity.this);
        dialogDate.setHint("Data: yyyy-mm-dd");
        layout.addView(dialogDate);

        final EditText dialogRating = new EditText(DataBaseActivity.this);
        dialogRating.setHint("from 1 to 10");
        layout.addView(dialogRating);


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(DataBaseActivity.this);
        alertDialog.setView(layout);
        alertDialog.setTitle("Dodaj film.")
                .setMessage("Podaj tytuł, budżet oraz datę premiery")
                .setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        title = dialogTitle.getText().toString();
                        monye = Integer.parseInt(dialogMoney.getText().toString());
                        date = dialogDate.getText().toString();
                        rating = dialogRating.getText().toString();

                        String rowNumber = polaczenieBazy.addMovie(title, monye, date, rating);

                        myRecycler.setAdapter(new MovieAdapter(polaczenieBazy.showMovies()));

                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).
                setCancelable(true).show();


    }

    @OnClick(R.id.buttonShow)
    public void showMovies() {
        List<MovieModel> allMovies = polaczenieBazy.showMovies();
        String result = "";
        for (MovieModel movie : allMovies) {
            result = result + "\n" + movie.toString();
        }

        if(result.equals("")){
            result = "Baza jest pusta!";
        }

    }

    @OnClick(R.id.buttonDelete)
    public void deleteMovie() {

        LinearLayout layout = new LinearLayout(DataBaseActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final Button deleteButton = new Button(DataBaseActivity.this);
        deleteButton.setText("!!usuń całą baze!!");
        layout.addView(deleteButton);


        final EditText dialogDeleteId = new EditText(DataBaseActivity.this);
        dialogDeleteId.setHint("lub podaj ID do usunięcia.");
        layout.addView(dialogDeleteId);


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(DataBaseActivity.this);
        alertDialog.setView(layout);
        alertDialog.setTitle("Usuwanie filmu.").
                setPositiveButton("Usuń", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String id = dialogDeleteId.getText().toString();
                        polaczenieBazy.deleteMovie(id);
                        showMovies();
                    }
                }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog helpDialog = alertDialog.show();

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                polaczenieBazy.cleanAllData();
                Toast.makeText(DataBaseActivity.this, "Baza danych została wyczyszczona.",
                        Toast.LENGTH_SHORT).show();
                helpDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.buttonEdit)
    public void updateMovie(){
        LinearLayout layout = new LinearLayout(DataBaseActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText dialogEditId = new EditText(DataBaseActivity.this);
        dialogEditId.setHint("Podaj ID do edycji.");
        layout.addView(dialogEditId);

        final EditText dialogEditTitle = new EditText(DataBaseActivity.this);
        dialogEditTitle.setHint("Możesz podać nowy tytuł.");
        layout.addView(dialogEditTitle);

        final EditText dialogNewMoney = new EditText(DataBaseActivity.this);
        dialogNewMoney.setHint("Możesz podać nowy budżet.");
        layout.addView(dialogNewMoney);

        final EditText dialogNewDate = new EditText(DataBaseActivity.this);
        dialogNewDate.setHint("Możesz podać nową datę.");
        layout.addView(dialogNewDate);

        final EditText dialogNewRating = new EditText(DataBaseActivity.this);
        dialogNewRating.setHint("Możesz podać nową ocnę.");
        layout.addView(dialogNewRating);

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(DataBaseActivity.this);
        alertDialog.setView(layout);
        alertDialog.setTitle("Edycja filmu.").
                setPositiveButton("Akceptuj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String id = dialogEditId.getText().toString();
                        String newTile = dialogEditTitle.getText().toString();
                        String newMoney = dialogNewMoney.getText().toString();
                        String newDate = dialogNewDate.getText().toString();
                        String newRating = dialogNewRating.getText().toString();

                        polaczenieBazy.editMovie(id,newTile, newMoney, newDate, newRating);
                        showMovies();
                    }
                }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        ButterKnife.bind(this);

        polaczenieBazy = new DataBaseQueries(DataBaseActivity.this);
        polaczenieBazy.openSave();
        polaczenieBazy.openRead();

        movieModelList = polaczenieBazy.showMovies();

        myRecycler = (RecyclerView) findViewById(R.id.myRecycleView);
        llm = new LinearLayoutManager(DataBaseActivity.this);
        myRecycler.setLayoutManager(llm);
        adapter = new MovieAdapter(movieModelList);
        myRecycler.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        polaczenieBazy.closeDB();
        super.onDestroy();
    }
}
