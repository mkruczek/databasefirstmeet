package pl.sdacademy.databasefirstmeet;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by RENT on 2017-07-04.
 */

public class HelperDB extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "DataMovie.db";


    public static final String SQL_CREATE_ENTRIES =
                    "CREATE TABLE " + MovieContract.FilmTabela.TABLE_NAME +
                    " (" + MovieContract.FilmTabela._ID + " INTEGER PRIMARY KEY,"
                    + MovieContract.FilmTabela.COLUMN_TITLE + " TEXT,"
                    + MovieContract.FilmTabela.COLUMN_MONEY + " INTEGER,"
                    + MovieContract.FilmTabela.COLUMN_YER + " TEXT,"
                    + MovieContract.FilmTabela.COLUMN_RATING + " TEXT)";

    public static final String SQL_DROP = "DROP TABLE IF EXISTS " + MovieContract.FilmTabela.TABLE_NAME;

    public HelperDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP);
        onCreate(db);
    }
}
