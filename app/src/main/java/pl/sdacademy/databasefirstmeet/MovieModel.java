package pl.sdacademy.databasefirstmeet;

/**
 * Created by RENT on 2017-07-05.
 */

public class MovieModel {

    private int id;
    private String title;
    private int money;
    private String yer;
    private String rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getYer() {
        return yer;
    }

    public void setYer(String yer) {
        this.yer = yer;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String score) {
        this.rating = score;
    }

    @Override
    public String toString(){

        return "id "+ getId() + ": " +getTitle() + " o budżecie " + getMoney() + " z roku: " + getYer();

    }
}
