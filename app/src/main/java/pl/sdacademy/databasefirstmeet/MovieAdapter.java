package pl.sdacademy.databasefirstmeet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by RENT on 2017-07-06.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    DataBaseQueries polaczenieBazy;
    private List<MovieModel> movieModelList;

    public MovieAdapter(List<MovieModel> movieModelList) {
        this.movieModelList = movieModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.one_row, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final int helperPosition = position + 1;

        MovieModel mm = movieModelList.get(position);
        holder.editTextMovie.setText(mm.toString());
        holder.score.setText(mm.getRating());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout layout = new LinearLayout(v.getContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText dialogEditTitle = new EditText(v.getContext());
                dialogEditTitle.setHint("Możesz podać nowy tytuł.");
                layout.addView(dialogEditTitle);

                final EditText dialogNewMoney = new EditText(v.getContext());
                dialogNewMoney.setHint("Możesz podać nowy budżet.");
                layout.addView(dialogNewMoney);

                final EditText dialogNewDate = new EditText(v.getContext());
                dialogNewDate.setHint("Możesz podać nową datę.");
                layout.addView(dialogNewDate);

                final EditText dialogNewRating = new EditText(v.getContext());
                dialogNewRating.setHint("Możesz podać nową ocnę.");
                layout.addView(dialogNewRating);

                final Context context = v.getContext();
                final View helperView = v;

                final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(v.getContext());
                alertDialog.setView(layout);
                alertDialog.setTitle("Edycja filmu.").
                        setPositiveButton("Akceptuj", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final String helpPosition = String.valueOf(helperPosition);
                                String newTile = dialogEditTitle.getText().toString();
                                String newMoney = dialogNewMoney.getText().toString();
                                String newDate = dialogNewDate.getText().toString();
                                String newRating = dialogNewRating.getText().toString();


                                polaczenieBazy = new DataBaseQueries(context);
                                polaczenieBazy.openSave();
                                polaczenieBazy.editMovie(helpPosition, newTile, newMoney, newDate, newRating);

                                movieModelList = polaczenieBazy.showMovies();
                                notifyDataSetChanged();


//                                movieModelList = polaczenieBazy.showMovies();
//                                RecyclerView myRecycler = (RecyclerView) helperView.findViewById(R.id.myRecycleView);
//                                LinearLayoutManager llm = new LinearLayoutManager(context);
//                                myRecycler.setLayoutManager(llm);
//                                MovieAdapter adapter = new MovieAdapter(movieModelList);
//                                myRecycler.setAdapter(adapter);


                            }
                        }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();


            }
        });
    }

    @Override
    public int getItemCount() {
        return movieModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView icon;
        public TextView editTextMovie;
        public TextView score;


        public MyViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.imageViewIcon);
            editTextMovie = (TextView) view.findViewById(R.id.editTextMovie);
            score = (TextView) view.findViewById(R.id.editTextscore);
        }


    }
}
