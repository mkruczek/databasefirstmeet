package pl.sdacademy.databasefirstmeet;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-07-04.
 */

public final class MovieContract {

    private MovieContract(){}

    public final class FilmTabela implements BaseColumns{

        public static final String TABLE_NAME = "Film";
        public static final String COLUMN_TITLE = "tytul";
        public static final String COLUMN_MONEY = "budzet";
        public static final String COLUMN_YER = "rok_powstania";
        public static final String COLUMN_RATING = "ocena";

    }


}
