package pl.sdacademy.databasefirstmeet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-04.
 */

public class DataBaseQueries {

    private SQLiteDatabase database;
    private HelperDB helperDB;

    public DataBaseQueries(Context context) {
        helperDB = new HelperDB(context);
    }

    public void openSave() throws SQLException{
        database = helperDB.getWritableDatabase();
    }

    public  void openRead() throws SQLException{
        database = helperDB.getReadableDatabase();
    }

    public void closeDB(){
        database.close();
    }
    
    public String addMovie (String nameMovie, int money, String yer, String rating){
        ContentValues values = new ContentValues();
        values.put(MovieContract.FilmTabela.COLUMN_TITLE, nameMovie);
        values.put(MovieContract.FilmTabela.COLUMN_MONEY, money);
        values.put(MovieContract.FilmTabela.COLUMN_YER,  yer);
        values.put(MovieContract.FilmTabela.COLUMN_RATING, rating);

        long rowNumber = database.insert(MovieContract.FilmTabela.TABLE_NAME, null, values);
        return String.valueOf(rowNumber);
    }

    public List<MovieModel> showMovies(){

//        MOJE WYPOCINY, były ok.
//        String[] movies = {MovieContract.FilmTabela._ID, MovieContract.FilmTabela.COLUMN_TITLE,
//                MovieContract.FilmTabela.COLUMN_MONEY, MovieContract.FilmTabela.COLUMN_YER};
//        String selection = MovieContract.FilmTabela.COLUMN_TITLE + " = ?";
//        String[] selectionMovie = {"My tilte"};
//        String sortOrder = MovieContract.FilmTabela.COLUMN_TITLE + " DESC";
//
//        Cursor cursor = database.query(
//                MovieContract.FilmTabela.TABLE_NAME,
//                movies, selection, selectionMovie, null, null, sortOrder);

        ArrayList<MovieModel> movies = new ArrayList<>();

        Cursor cursor = database.query(MovieContract.FilmTabela.TABLE_NAME, null, null, null, null, null, null);

        MovieModel mm;
        if(cursor.getCount() > 0){
            for (int i = 0; i < cursor.getCount(); i++){
                cursor.moveToNext();
                mm = new MovieModel();
                mm.setId(cursor.getInt(0));
                mm.setTitle(cursor.getString(1));
                mm.setMoney(cursor.getInt(2));
                mm.setYer(cursor.getString(3));
                mm.setRating(cursor.getString(4));

                movies.add(mm);
            }
        }

        return movies;
    }

    public void deleteMovie (String id){
        String selection = MovieContract.FilmTabela._ID + " = ?";
        String[] selectionsArgs = { id };

        database.delete(MovieContract.FilmTabela.TABLE_NAME, selection, selectionsArgs);
    }

    public void editMovie(String id, String title, String money, String yer, String rating){
        ContentValues values = new ContentValues();
        if(!title.equals("")) {
            values.put(MovieContract.FilmTabela.COLUMN_TITLE, title);
        }
        if(!money.equals("")) {
            values.put(MovieContract.FilmTabela.COLUMN_MONEY, Integer.valueOf(money));
        }
        if(!yer.equals("")) {
            values.put(MovieContract.FilmTabela.COLUMN_YER, yer);
        }

        if(!rating.equals("")) {
            values.put(MovieContract.FilmTabela.COLUMN_RATING, rating);
        }

        String selection = MovieContract.FilmTabela._ID + " = ?";
        String[] selectionsArgs = { id };

        database.update(MovieContract.FilmTabela.TABLE_NAME, values, selection, selectionsArgs);

    }

    public void cleanAllData(){
        database.execSQL("DELETE FROM " + MovieContract.FilmTabela.TABLE_NAME);
    }
}
